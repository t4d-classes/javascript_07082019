
// named export
export const doIt = () => {
  console.log('did it');
};

export const doIt2 = () => {
  console.log('did it 2');
};

export const doIt3 = () => {
  console.log('did it 3');
};


export default {
  doIt, doIt2, doIt3
};