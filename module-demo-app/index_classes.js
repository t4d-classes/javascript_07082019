
// function Person(fn, ln) {
//   this.fn = fn;
//   this.ln = ln;
// }

// Person.prototype.getFullName = function() {
//   return this.fn + ' ' + this.ln;
// };


// function Student(studentId, fn, ln) {
//   this._super(fn, ln);
//   this.studentId = studentId;
// }

// Student.prototype = Object.create(Person.prototype);
// Student.prototype.constructor = Student;
// Student.prototype._super = Person;

// Student.prototype.getRecordInfo = function() {
//   return this.studentId + ' ' + this.ln + ', ' + this.fn;
// };

// const p = new Student(1, 'Bob', 'Smith');

// console.log(p);
// console.log(p.getFullName());

class Person2 {

  constructor(fn, ln) {
    this.fn = fn;
    this.ln = ln;
  }

  getFullName() {
    return this.fn + ' ' + this.ln;
  }

}

class Student2 extends Person2 {

  constructor(studentId, fn, ln) {
    super(fn, ln);
    this.studentId = studentId;

    this.getRecordInfo = this.getRecordInfo.bind(this);
  }

  getRecordInfo() {
    console.log(this.studentId + ' ' + this.ln + ', ' + this.fn);
  }

  otherFunc() {
    setTimeout(this.getRecordInfo, 3000);
  }

}

const s2 = new Student2(1, 'Sally', 'Smith');

// console.log(s2);
// console.log(s2.getFullName());
// console.log(s2.getRecordInfo());

s2.otherFunc();

console.log(s2);